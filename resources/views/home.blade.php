<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">



    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">

</head>

<body class="antialiased">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin:24px 0;">
        <a class="navbar-brand">{{$usuario[0]->NOMBRES}} {{$usuario[0]->APELLIDOS}}</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navb">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Login</a>
                </li>
                <li class="nav-item">

                </li>
                <li class="nav-item">

                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">

                <button onclick="modal()" type="button" class="btn btn-outline-secondary">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart-check" viewBox="0 0 16 16">
                        <path d="M11.354 6.354a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"></path>
                        <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"></path>
                    </svg>
                    <label style="color:white" id="lblNumero">0</label>
                </button>

            </form>
        </div>
    </nav>

    <br>
    <div class="container">

        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">

                <form method="POST" action="{{ url('api/usuario/crear') }}" class="was-validated">

                    <div class="row">

                        @foreach ($producto as $user)

                        <div class="col-md-6">
                            <div class="card" style="width:300px">
                                <img class="card-img-top" src="{{$user->IMAGEN}}" alt="Card image" style="width:100%">
                                <div class="card-body">
                                    <h4 class="card-title">
                                        {{$user->NOMBRE}}
                                    </h4>
                                    <p class="card-text">
                                        {{$user->DESCRIPCION}}
                                    </p>
                                    <a onclick="seleccionar('{{$user}}')" class="btn btn-primary">Seleccionar</a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                </form>

            </div>
            <div class="card-footer"></div>
        </div>

    </div>


    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Mis Productos</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <table>
                        <thead>
                            <th>
                            </th>
                            <th>
                                PRODUCTO
                            </th>
                            <th>
                                NUMERO
                            </th>
                            <th>
                            </th>
                        </thead>
                        <tbody id="panelProducto">

                        </tbody>

                    </table>



                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <script>
        var numero = [];

        function seleccionar(id) {

            numero.push(JSON.parse(id));
            $("#lblNumero").text(numero.length);
        }

        function modal() {
            $("#myModal").modal();


            numero.forEach(element => {

                $("#panelProducto").append(`<tr>  <td> <img width="60px" src='${element.IMAGEN}'> </td> <td> ${element.NOMBRE} </td> <td>  1  </td>  <td> <button onclick="eliminar(${element.id})" type="button" class="btn btn-danger">Eliminar</button> </td>   </tr>`)

            });

        }

        function eliminar(id) {

            var nuevo = [];

            $("#panelProducto").html("");
            numero.forEach(element => {
                if (element.id != id) {
                    $("#panelProducto").append(`<tr>  <td> <img width="60px" src='${element.IMAGEN}'> </td> <td> ${element.NOMBRE} </td> <td>  1  </td>  <td> <button onclick="eliminar(${element.id})" type="button" class="btn btn-danger">Eliminar</button> </td>   </tr>`);

                    nuevo.push(element);
                }

            });

            numero=nuevo;

        }
    </script>


</body>

</html>