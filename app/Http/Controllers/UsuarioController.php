<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Producto;
use Exception;

class UsuarioController extends Controller
{

    public function login(Request $request)
    {
        $return = ['status' => 200, 'message' => 'OK', 'data' => [], 'producto' => []];

        try {

            $usuario = new Usuario();
            $result = $usuario::where('CORREO', $request->email)
                ->where('CONTRASENIA', $request->pwd)
                ->get();

            if (count($result) > 0) {

                $producto = new Producto();
                $return['producto'] = $producto::all();

                $return['usuario'] = $result;

                return view('home', $return);
            } else {
                return view('welcome', ["mensaje" => "Usuario incorrecto"]);
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


    public function crear(Request $request)
    {
        $return = ['status' => 200, 'message' => 'OK', 'data' => ''];

        try {

            $usuario = new Usuario;
            $usuario->id = rand(5, 15);
            $usuario->NOMBRES = $request->nombre;
            $usuario->APELLIDOS = $request->apellido;
            $usuario->CORREO = $request->correo;
            $usuario->CONTRASENIA = $request->password;
            $usuario->save();

            $return['data'] = $usuario::all();
        } catch (\Throwable $th) {

            $return['message'] = $th->getMessage() . '|' . $th->getLine();
            $return['status'] = 400;
        }

        return view("usuario", $return);
    }


    public function init()
    {
        $return = ['status' => 200, 'message' => 'OK', 'data' => ''];

        try {

            $usuario = new Usuario;
            $return['data'] = $usuario::all();
        } catch (\Throwable $th) {

            $return['message'] = $th->getMessage() . '|' . $th->getLine();
            $return['status'] = 400;
        }

        return view("usuario", $return);
    }

    public function eliminar($id)
    {
        $return = ['status' => 200, 'message' => 'OK', 'data' => ''];

        try {

            $user = Usuario::find($id);
            $user->delete();

            $usuario = new Usuario;
            $return['data'] = $usuario::all();
        } catch (\Throwable $th) {

            $return['message'] = $th->getMessage() . '|' . $th->getLine();
            $return['status'] = 400;
        }

        return view("usuario", $return);
    }

    public function seleccionar($id)
    {
        $return = ['status' => 200, 'message' => 'OK', 'data' => ''];

        try {

            $producto = new Usuario();
            $result = $producto::where('id', $id)
                ->get();

            $return['data'] =  $result;
        } catch (\Throwable $th) {

            $return['message'] = $th->getMessage() . '|' . $th->getLine();
            $return['status'] = 400;
        }

        return view("actualizar", $return);
    }

    public function actualizar(Request $request)
    {
        $return = ['status' => 200, 'message' => 'Registro actualizado', 'data' => ''];

        try {

            Usuario::where('id', $request->id)
                ->update([
                    'NOMBRES' => $request->nombre,
                    'APELLIDOS' => $request->apellido,
                    'CORREO' => $request->correo,
                    'CONTRASENIA' => $request->password,
                ]);

            $usuario = new Usuario;
            $return['data'] = $usuario::all();
        } catch (Exception $th) {
            $return['message'] = $th->getMessage() . '|' . $th->getLine();
            $return['status'] = 400;
        }

        return view("usuario", $return);
    }
}
