<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;
use App\Models\Producto;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $usuario = new Usuario;
        $usuario->id = 1;
        $usuario->NOMBRES = 'Prueba';
        $usuario->APELLIDOS = 'Apellido';
        $usuario->CORREO = 'judaba94@outlook.com';
        $usuario->CONTRASENIA = '123';
        $usuario->save();


        $producto = new Producto;
        $producto->id = 1;
        $producto->NOMBRE = 'Televisor';
        $producto->DESCRIPCION = 'Producto con carantia con 1 año.';
        $producto->IMAGEN = 'https://cdn.alkst.co/t72a168ee/img/474xt_5g2mz9_f9e51bc5.jpg';
        $producto->save();

        $producto1 = new Producto;
        $producto1->id = 2;
        $producto1->NOMBRE = 'Lavadora';
        $producto1->DESCRIPCION = 'Producto con carantia con 1 año.';
        $producto1->IMAGEN = 'https://cdn.alkst.co/t72a168ee/img/474xt_s8znkf_9dcd23fd.jpg';
        $producto1->save();

        $producto2 = new Producto;
        $producto2->id = 3;
        $producto2->NOMBRE = 'Computadora';
        $producto2->DESCRIPCION = 'Producto con carantia con 1 año.';
        $producto2->IMAGEN = 'https://cdn.alkst.co/t72a168ee/img/474xt_r2a3fw_ee1399ed.jpg';
        $producto2->save();

        $producto3 = new Producto;
        $producto3->id = 4;
        $producto3->NOMBRE = 'Telefono';
        $producto3->DESCRIPCION = 'Producto con carantia con 1 año.';
        $producto3->IMAGEN = 'https://cdn.alkst.co/t72a168ee/img/474xt_3un7cv_79a23c89.jpg';
        $producto3->save();
    }
}
