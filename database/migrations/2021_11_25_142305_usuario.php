<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Usuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('USUARIO', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('NOMBRES', 255)->nullable();
            $table->string('APELLIDOS', 255)->nullable();
            $table->string('CORREO', 255)->nullable();
            $table->string('CONTRASENIA', 255)->nullable();
            $table->boolean('STATES')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('USUARIO');
    }
}
