<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Producto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PRODUCTO', function (Blueprint $table) {
            $table->id();
            $table->string('NOMBRE', 255)->nullable();
            $table->string('DESCRIPCION', 1000)->nullable();
            $table->string('IMAGEN', 255)->nullable();
            $table->boolean('STATES')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PRODUCTO');
    }
}
